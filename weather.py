#!/usr/bin/env python3
import requests, json
baseurl = 'http://api.openweathermap.org/data/2.5/weather'
apikey = '2229c99d9af49a4df925893d5e17d0f1'

input_string = input("Enter a list element separated by space: ")
cites = input_string.split()

for city in cites:
    res = requests.get('{}?q={}&units=metric&appid={}'.format(baseurl,city,apikey))
    w = json.loads(res.text)
    tempnew = round((w['main']['temp']))
    humiditynew = w['main']['humidity']
    print ('{} {}\u2013 {} ' .format(city,tempnew,humiditynew))
